﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Concurrent;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace ServerStatus
{
    public enum DatabaseStatus
    {
        NotConnected,
        Connecting,
        Connected
    }

    public class QueryResult : IDisposable
    {
        public bool Successful { get; set; }
        public DbDataReader Reader { get; set; }
        public MySqlException Exception { get; set; }
        public long LastInsertID { get; set; }
        public int AffectedRows { get; set; }

        public QueryResult()
        {
            Successful = false;
        }

        ~QueryResult()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && Reader != null)
            {
                Reader.Dispose();
                Reader = null;
            }
        }
    }

    public sealed class Database : INotifyPropertyChanged, IDisposable
    {
        private static DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0);
        private static List<ConnectionState> InvalidConnectionStates = new List<ConnectionState>() { ConnectionState.Broken, ConnectionState.Closed };

        private DatabaseStatus _status = DatabaseStatus.NotConnected;
        private MySqlConnection _connection = null;
        private SemaphoreSlim _connectionSemaphore = new SemaphoreSlim(1);
        
        public event PropertyChangedEventHandler PropertyChanged;

        public DatabaseStatus Status
        {
            get
            {
                return _status;
            }

            private set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        private string _connectionString
        {
            get
            {
                return String.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3};", App.Settings.MysqlHost, App.Settings.MysqlName, App.Settings.MysqlUser, App.Settings.MysqlPass);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool IsConnectionInvalid()
        {
            return _connection == null || InvalidConnectionStates.Contains(_connection.State) || !_connection.Ping();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._connection != null)
                {
                    this._connection.Dispose();
                }

                if (this._connectionSemaphore != null)
                {
                    this._connectionSemaphore.Dispose();
                }
            }
        }

        private async Task<MySqlCommand> ObtainCommand(string query)
        {
            await _connectionSemaphore.WaitAsync();

            while (IsConnectionInvalid())
            {
                try
                {
                    Status = DatabaseStatus.Connecting;
                    _connection = new MySqlConnection(_connectionString);

                    await _connection.OpenAsync();

                    if (_connection.State == ConnectionState.Open)
                    {
                        Status = DatabaseStatus.Connected;
                        continue;
                    }
                    else
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                catch (MySqlException e)
                {
                    LogException(e);
                }

                await Task.Delay(15000);
            }

            return new MySqlCommand(query, _connection);
        }

        public async Task<QueryResult> Execute(string queryString, bool obtainReader = false)
        {
            QueryResult qr = new QueryResult();

            try
            {
                using (MySqlCommand cmd = await ObtainCommand(queryString))
                {
                    if (obtainReader)
                    {
                        qr.Reader = await cmd.ExecuteReaderAsync();
                    }
                    else
                    {
                        qr.AffectedRows = await cmd.ExecuteNonQueryAsync();
                    }

                    qr.Successful = true;
                    qr.LastInsertID = cmd.LastInsertedId;
                }
            }
            catch (MySqlException e)
            {
                qr.Exception = e;
                LogException(e);
            }
            catch (Exception e)
            {
                Log.Error(String.Format("Database query failed due to a {0}{3}Message: {1}{3}{2}", e.GetType().FullName, e.Message, e.StackTrace, Environment.NewLine));
            }
            finally
            {
                _connectionSemaphore.Release();
            }

            if (!qr.Successful)
            {
                Log.Error("The query string was:" + Environment.NewLine + queryString);
            }

            return qr;
        }

        private void LogException(Exception ex)
        {
            if (ex is MySqlException)
            {
                Log.Error("MySqlException (SQL #" + (ex as MySqlException).Number + ")");
            }
            else
            {
                Log.Error(ex.GetType().FullName);
            }

            Log.Error(ex.Message);
            Log.Error(ex.StackTrace);
        }

        private string _tableCreationQueryString = @"SET SQL_MODE = ""NO_AUTO_VALUE_ON_ZERO"";
SET time_zone = ""+00:00"";

CREATE TABLE IF NOT EXISTS `status_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` int(10) unsigned NOT NULL,
  `pinger` int(10) unsigned NOT NULL,
  `timestamp` int(10) unsigned NOT NULL,
  `successful` tinyint(1) NOT NULL,
  `elapsed` int(11) NOT NULL DEFAULT '-1',
  `failure_message` tinytext,
  PRIMARY KEY (`id`),
  KEY `server` (`server`),
  KEY `pinger` (`pinger`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `status_pingers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` int(10) unsigned NOT NULL,
  `type` varchar(128) NOT NULL,
  `hostname` varchar(64) NOT NULL,
  `timeout` int(11) NOT NULL,
  `port` int(11) DEFAULT NULL,
  `label` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `status_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `status_history`
  ADD CONSTRAINT `history_pingers_id` FOREIGN KEY (`pinger`) REFERENCES `status_pingers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `history_servers_id` FOREIGN KEY (`server`) REFERENCES `status_servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `status_pingers`
  ADD CONSTRAINT `pingers_servers_id` FOREIGN KEY (`server`) REFERENCES `status_servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;";

        private string _testSelectQueryString = @"SELECT id FROM status_history LIMIT 1; SELECT id FROM status_pingers LIMIT 1; SELECT id FROM status_servers LIMIT 1;";

        private async Task CreateTables()
        {
            using (QueryResult qr = await Execute(_testSelectQueryString))
            {
                if (qr.Successful)
                {
                    // all tables already exist
                    return;
                }
            }

            Log.Warning("Database tables do not exist, creating.");

            using (QueryResult qr = await Execute(_tableCreationQueryString))
            {
                if (qr.Successful)
                {
                    Log.Info("Database structure created.");
                }
            }
        }

        public async Task LoadStuff()
        {
            await CreateTables();
            await LoadServers();
            LoadPingers();
        }

        private async Task LoadServers()
        {
            using (QueryResult qr = await Execute("SELECT id, name FROM status_servers", true))
            {
                try
                {
                    while (qr.Reader.Read())
                    {
                        Server server = new Server();
                        server.ID = (uint)qr.Reader.GetInt32(0);
                        server.Name = qr.Reader.GetString(1);
                        server.EnableSaving();

                        App.Holder.Servers.Add(server);
                    }
                }
                catch (Exception e)
                {
                    LogException(e);
                }
            }
        }

        private async void LoadPingers()
        {
            foreach (Server server in App.Holder.Servers)
            {
                using (QueryResult qr = await Execute("SELECT id, type, hostname, timeout, port, name FROM status_pingers WHERE server=" + server.ID, true))
                {
                    try
                    {
                        while (qr.Reader.Read())
                        {
                            string pingerType = qr.Reader.GetString(1);

                            if (App.Holder.PingerTypes.Contains(pingerType))
                            {
                                Type handle = Type.GetType(pingerType);

                                Pinger p = handle.GetConstructor(Type.EmptyTypes).Invoke(null) as Pinger;
                                p.ID = qr.Reader.GetInt32(0);
                                p.Hostname = qr.Reader.GetString(2);
                                p.Timeout = (ushort)qr.Reader.GetInt16(3);
                                p.Name = qr.Reader.GetString(5);

                                if (handle.GetProperty("Port") != null)
                                {
                                    handle.GetProperty("Port").SetValue(p, (ushort)qr.Reader.GetInt16(4));
                                }

                                server.Pingers.Add(p);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LogException(e);
                    }
                }
            }
        }

        public async void AddServer(string label, bool recursing = false)
        {
            using (QueryResult qr = await Execute("INSERT INTO status_servers (name) VALUES ('" + MySqlHelper.EscapeString(label) + "')"))
            {
                if (qr.Successful) {
                    Server s = new Server()
                    {
                        ID = (uint)qr.LastInsertID,
                        Name = label
                    };

                    s.EnableSaving();
                    App.Holder.Servers.Add(s);
                }
            }
        }

        public async void AddPinger(string fullPingerTypeName, string name, Server server)
        {
            Type pingerTypeHandle = Type.GetType(fullPingerTypeName);
            Pinger instance = pingerTypeHandle.GetConstructor(Type.EmptyTypes).Invoke(null) as Pinger;

            string insertQueryString = String.Format(
                @"INSERT INTO status_pingers (server, type, hostname, timeout, port, label, name)
                VALUES ('{0}', '{1}', 'localhost', 30, {2}, '{3}', '{4}')",
                                                            
                server.ID,
                MySqlHelper.EscapeString(fullPingerTypeName),
                fullPingerTypeName == typeof(TCPPinger).FullName ? "80" : "NULL",
                MySqlHelper.EscapeString(instance.Label),
                MySqlHelper.EscapeString(name)
            );

            using (QueryResult qr = await Execute(insertQueryString))
            {

                instance.ID = (int)qr.LastInsertID;
                instance.Hostname = "localhost";
                instance.Timeout = 30;
                instance.Name = name;

                PropertyInfo portPropertyInfo;
                if ((portPropertyInfo = pingerTypeHandle.GetProperty("Port")) != null)
                {
                    portPropertyInfo.SetValue(instance, (uint)80);
                }

                server.Pingers.Add(instance);
            }
        }

        public async void UpdateServer(Server server)
        {
            string updateQueryString = String.Format(
                @"UPDATE status_servers
                SET name='{0}'
                WHERE id={1}",

                MySqlHelper.EscapeString(server.Name),
                server.ID
            );

            using (QueryResult qr = await Execute(updateQueryString))
            {
                if (!qr.Successful || qr.AffectedRows != 1)
                {
                    Log.Warning(
                        String.Format(
                            "Couldn't update server #{0} to name '{1}'; the query affected {2} rows (expected: 1).",
                            server.ID,
                            server.Name,
                            qr.AffectedRows
                        )
                    );
                }

                server.IsBeingSaved = false;
            }
        }

        public async void UpdatePinger(Pinger pinger)
        {
            string updateQueryString = String.Format(
                @"UPDATE status_pingers
                SET hostname='{0}', timeout='{1}', port={2}, name='{3}'
                WHERE id={4}
                LIMIT 1",
                                                            
                MySqlHelper.EscapeString(pinger.Hostname),
                pinger.Timeout,
                pinger is TCPPinger ? (pinger as TCPPinger).Port.ToString() : "NULL",
                MySqlHelper.EscapeString(pinger.Name),
                pinger.ID
            );

            using (QueryResult qr = await Execute(updateQueryString))
            {
                if (!qr.Successful || qr.AffectedRows != 1)
                {
                    Log.Warning(
                        String.Format(
                            "Couldn't update pinger #{0} to hostname '{1}', name '{5}', timeout '{2}' and port '{3}'; the query affected {4} rows (expected: 1).",
                            pinger.ID,
                            pinger.Hostname,
                            pinger.Timeout,
                            pinger is TCPPinger ? (pinger as TCPPinger).Port.ToString() : "null",
                            qr.AffectedRows,
                            pinger.Name
                        )
                    );
                }
            }
        }

        public async void DeleteServer(Server server)
        {
            using (QueryResult qr = await Execute("DELETE FROM status_servers WHERE id=" + server.ID + " LIMIT 1"))
            {
                if (qr.Successful && qr.AffectedRows == 1)
                {
                    App.Holder.Servers.Remove(server);
                }
                else
                {
                    Log.Warning("Couldn't delete server #" + server.ID + ": query affected " + qr.AffectedRows + " (expected: 1).");
                }
            }
        }

        public async void DeletePinger(Server owner, Pinger pinger)
        {
            using (QueryResult qr = await Execute("DELETE FROM status_pingers WHERE id=" + pinger.ID + " LIMIT 1"))
            {
                if (qr.Successful && qr.AffectedRows == 1 && owner != null)
                {
                    owner.Pingers.Remove(pinger);
                }
                else
                {
                    Log.Warning("Couldn't delete pinger #" + pinger.ID + ": the query affected " + qr.AffectedRows + " (expected: 1).");
                }
            }
        }

        public async void AddPingerResult(Server server, Pinger pinger, PingerResult result, int recursionDepth = 0)
        {
            string insertQueryString = String.Format(
                @"INSERT INTO status_history (server, timestamp, pinger, successful, elapsed, failure_message)
                VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",

                server.ID,
                (int)Math.Round(result.When.Subtract(UnixEpoch).TotalSeconds),
                pinger.ID,
                result.Successful ? 1 : 0,
                result.Elapsed,
                result.Successful ? "NULL" : result.ErrorMessage
            );

            using (QueryResult qr = await Execute(insertQueryString))
            {
                if (!qr.Successful)
                {
                    Log.Error(
                        String.Format(
                            "Couldn't add result for {0} pinger #{1} (server #{2} - {3}) - query failed.",
                            pinger.Label,
                            pinger.ID,
                            server.ID,
                            server.Name
                        )
                    );
                }
            }
        }
    }
}
