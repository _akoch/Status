﻿using System;

namespace ServerStatus
{
    public class PingerResult
    {
        public bool Successful { get; set; }
        public DateTime When { get; set; }
        public int Elapsed { get; set; }
        public string ErrorMessage { get; set; }

        public PingerResult(bool successful, int elapsed)
        {
            this.Successful = successful;
            this.Elapsed = elapsed;
            this.When = DateTime.UtcNow;
            this.ErrorMessage = null;
        }

        public PingerResult(bool successful, string errorMsg)
        {
            this.Successful = successful;
            this.Elapsed = -1;
            this.When = DateTime.UtcNow;
            this.ErrorMessage = errorMsg;
        }
    }
}
