﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using System.Diagnostics;

namespace ServerStatus
{
    public class Daemon : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected Thread workerThread = null;
        protected Thread watcherThread = null;
        protected bool wasWorkerRunning = false;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected bool IsThreadRunning(Thread t)
        {
            if (t == null)
            {
                return false;
            }
            else
            {
                return t.IsAlive;
            }
        }

        public bool IsWorkerRunning
        {
            get
            {
                return IsThreadRunning(workerThread);
            }
        }

        public bool IsWatcherRunning
        {
            get
            {
                return IsThreadRunning(watcherThread);
            }
        }

        public void Start()
        {
            if (!IsWorkerRunning)
            {
                workerThread = new Thread(new ThreadStart(DoWork));
                workerThread.IsBackground = true;
                workerThread.Start();
            }

            if (!this.IsWatcherRunning)
            {
                watcherThread = new Thread(new ThreadStart(WatchWorker));
                watcherThread.IsBackground = true;
                watcherThread.Start();
            }
        }

        public void Stop()
        {
            if (IsWorkerRunning)
            {
                workerThread.Abort();
            }

            if (IsWatcherRunning)
            {
                watcherThread.Abort();
            }

            Task.Run(() =>
            {
                Thread.Sleep(500);
                ReportWorkerStatusChanged();
                ReportWatcherStatusChanged();
            });
        }

        protected void DoWork()
        {
            List<Server> servers;

            while (true)
            {
                lock (App.Holder.Servers)
                {
                    servers = new List<Server>(App.Holder.Servers);
                }

                if (servers.Count > 0)
                {
                    int pingerCount;
                    int successfulPingers;

                    foreach (Server s in servers) {
                        Monitor.Enter(s);

                        s.Status = Status.QueryInProgress;

                        pingerCount = s.Pingers.Count;

                        if (pingerCount == 0)
                        {
                            s.Status = Status.NotAvailable;
                            continue;
                        }
                        
                        successfulPingers = 0;

                        foreach (Pinger p in s.Pingers)
                        {
                            p.Process();

                            if (p.LastResult != null)
                            {
                                PingerResult result = p.LastResult;

                                if (result.Successful)
                                {
                                    successfulPingers++;
                                }

                                App.Database.AddPingerResult(s, p, result);
                            }
                        }

                        if (successfulPingers == 0)
                        {
                            s.Status = Status.FullOutage;
                        }
                        else if (successfulPingers < pingerCount)
                        {
                            s.Status = Status.PartialOutage;
                        }
                        else
                        {
                            s.Status = Status.Online;
                        }

                        Monitor.Exit(s);
                    }
                }

                Thread.Sleep(TimeSpan.FromSeconds(App.Settings.Interval));
            }
        }

        

        protected void ReportWorkerStatusChanged()
        {
            OnPropertyChanged("IsWorkerRunning");
            CommandManager.InvalidateRequerySuggested();

            Task.Run(async () =>
            {
                await Task.Delay(500);
                App.Current.Dispatcher.Invoke(() =>
                {
                    CommandManager.InvalidateRequerySuggested();
                });
            });
        }

        protected void ReportWatcherStatusChanged()
        {
            OnPropertyChanged("IsWatcherRunning");
        }

        protected void WatchWorker()
        {
            wasWorkerRunning = false;
            ReportWatcherStatusChanged();

            while (true)
            {
                bool isWorkerRunning = false;

                lock (workerThread)
                {
                    isWorkerRunning = IsThreadRunning(workerThread);
                }

                if (isWorkerRunning ^ wasWorkerRunning)
                {
                    wasWorkerRunning = isWorkerRunning;
                    ReportWorkerStatusChanged();

                    if (!isWorkerRunning)
                    {
                        Task.Run(() =>
                        {
                            Thread.Sleep(500);
                            ReportWatcherStatusChanged();
                        });

                        // rip worker, we've got nothing to watch now
                        // ABORT THREAD.jpg
                        return;
                    }
                }

                Thread.Sleep(1500);
            }
        }
    }
}
