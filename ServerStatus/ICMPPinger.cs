﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Diagnostics;

namespace ServerStatus
{
    [DisplayName("ICMP")]
    [Serializable]
    public class ICMPPinger : Pinger
    {
        protected override PingerResult AttemptContact()
        {
            try
            {
                using (Ping sender = new System.Net.NetworkInformation.Ping())
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                        PingReply reply = sender.Send(this.Hostname, this.Timeout*1000);
                    stopwatch.Stop();

                    if (reply.Status == IPStatus.Success)
                    {
                        return new PingerResult(true, (int)stopwatch.ElapsedMilliseconds);
                    }
                    else
                    {
                        return new PingerResult(false, Enum.GetName(typeof(IPStatus), reply.Status));
                    }
                }
            }
            catch (PingException pe)
            {
                return new PingerResult(false, "Connection attempt halted by a PingException: " + pe.Message);
            }
            catch (Exception e)
            {
                return new PingerResult(false, "Daemon error - unhandled " + e.GetType().FullName + ": " + e.Message);
            }
        }
    }
}
