﻿using System;
using System.ComponentModel;

namespace ServerStatus
{
    static class Log
    {
        private static void Print(string level, string message)
        {
            string wat = String.Format("[{0}] {1}: {2}{3}", level, DateTime.Now.ToShortTimeString(), message, Environment.NewLine);
            System.Diagnostics.Debug.Write(wat);
            App.Holder.Log = wat + App.Holder.Log;
        }

        public static void Info(string message)
        {
            Print("INFO", message);
        }

        public static void Warning(string message) {
            Print("WARNING", message);
        }

        public static void Error(string message) {
            Print("ERROR", message);
        }
    }
}
