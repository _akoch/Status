﻿using System;
using System.Xml.Serialization;
using System.ComponentModel;

namespace ServerStatus
{
    [Serializable]
    public abstract class PortPinger : Pinger
    {
        [XmlIgnore]
        private uint _port;

        public uint Port
        {
            get
            {
                return _port;
            }

            set
            {
                _port = value;
                OnPropertyChanged("Port");
            }
        }
    }
}
