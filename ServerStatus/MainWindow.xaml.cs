﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerStatus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CanExecuteCommand(object sender, CanExecuteRoutedEventArgs args)
        {
            if (args.Command == NavigationCommands.Refresh)
            {
                args.CanExecute = App.Database.Status == DatabaseStatus.NotConnected;
            }
            else if (args.Command == ApplicationCommands.New)
            {
                args.CanExecute = true;
            }
            else if (args.Command == ApplicationCommands.Paste) {
                if (ServersListBox != null)
                {
                    args.CanExecute = ServersListBox.SelectedItem != null && ServersListBox.SelectedItems.Count == 1;
                }
            }
            else if (args.Command == ApplicationCommands.Replace)
            {
                args.CanExecute = ServersListBox.SelectedItem != null && ServersListBox.SelectedItems.Count == 1;
            }
            else if (args.Command == ApplicationCommands.Delete)
            {
                if (sender == ServersListBox)
                {
                    args.CanExecute = ServersListBox.SelectedItems.Count > 0;
                }
                else if (sender == PingersListBox)
                {
                    args.CanExecute = PingersListBox.SelectedItems.Count > 0;
                }
            }
            else if (args.Command == ServerStatusCommands.StartDaemon)
            {
                args.CanExecute = !App.Daemon.IsWorkerRunning;
            }
            else if (args.Command == ServerStatusCommands.StopDaemon)
            {
                args.CanExecute = App.Daemon.IsWorkerRunning;
            }
        }

        private void ExecuteCommand(object sender, ExecutedRoutedEventArgs args)
        {
            if (args.Command == NavigationCommands.Refresh)
            {
                args.Handled = true;
            }
            else if (args.Command == ApplicationCommands.New)
            {
                StringPrompt sp = StringPrompt.Create("Enter New Server Name:");

                if (sp.ShowDialog() == true)
                {
                    App.Database.AddServer(sp.PromptResult);
                }

                args.Handled = true;
            }
            else if (args.Command == ApplicationCommands.Paste)
            {
                foreach (KeyValuePair<Type, string> typeLabelPair in App.Holder.PingerTypeLabels)
                {
                    if (typeLabelPair.Value == args.Parameter.ToString())
                    {
                        StringPrompt sp = StringPrompt.Create("Enter New Pinger Name:");

                        if (sp.ShowDialog() == true)
                        {
                            App.Database.AddPinger(typeLabelPair.Key.FullName, sp.PromptResult, ServersListBox.SelectedItem as Server);
                        }

                        args.Handled = true;
                    }
                }
            }
            else if (args.Command == ApplicationCommands.Replace)
            {
                Server selected = ServersListBox.SelectedItem as Server;
                args.Handled = true;

                if (selected == null)
                {
                    return;
                }

                StringPrompt sp = StringPrompt.Create("Rename Server:", selected.Name);
                if (sp.ShowDialog() == true && selected != null)
                {
                    selected.Name = sp.PromptResult;
                    App.Database.UpdateServer(selected);
                }
            }
            else if (args.Command == ApplicationCommands.Delete)
            {
                if (sender == PingersListBox)
                {
                    args.Handled = true;

                    if (PingersListBox.SelectedItems.Count == 0)
                    {
                        return;
                    }

                    if (MessageBox.Show("Permanently delete " + PingersListBox.SelectedItems.Count + " pingers?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        while (PingersListBox.SelectedItems.Count > 0)
                        {
                            App.Database.DeletePinger(ServersListBox.SelectedValue as Server, PingersListBox.SelectedItems[0] as Pinger);
                        }
                    }
                }
                else
                {
                    args.Handled = true;

                    if (ServersListBox.SelectedItems.Count == 0)
                    {
                        return;
                    }

                    if (MessageBox.Show("Permanently delete " + ServersListBox.SelectedItems.Count + " servers?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        while (ServersListBox.SelectedItems.Count > 0)
                        {
                            App.Database.DeleteServer(ServersListBox.SelectedItems[0] as Server);
                        }
                    }
                }
            }
            else if (args.Command == ServerStatusCommands.StartDaemon)
            {
                args.Handled = true;
                App.Daemon.Start();
            }
            else if (args.Command == ServerStatusCommands.StopDaemon)
            {
                args.Handled = true;
                App.Daemon.Stop();
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await App.Database.LoadStuff();

            if (App.Settings.Autostart)
            {
                App.Daemon.Start();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Daemon.Start();
        }
    }
}
