﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerStatus
{
    public partial class StringPrompt : Window
    {
        public StringPrompt()
        {
            InitializeComponent();
        }

        public string PromptResult
        {
            get;
            protected set;
        }

        public static StringPrompt Create(string description)
        {
            StringPrompt sp = new StringPrompt();
            sp.PromptDescription.Content = description;
            sp.Owner = Application.Current.MainWindow;

            return sp;
        }

        public static StringPrompt Create(string description, string defaultValue)
        {
            StringPrompt sp = Create(description);
            sp.PromptValue.Text = defaultValue;

            return sp;
        }

        private void CanExecuteCommand(object sender, CanExecuteRoutedEventArgs args)
        {
            if (args.Command == ApplicationCommands.Stop)
            {
                args.CanExecute = true;
            }
            else if (args.Command == ApplicationCommands.Save)
            {
                args.CanExecute = PromptValue.Text.Trim().Length > 0;
            }
        }

        private void ExecuteCommand(object sender, ExecutedRoutedEventArgs args)
        {
            if (args.Command == ApplicationCommands.Stop)
            {
                this.DialogResult = false;
                this.Close();
            }
            else if (args.Command == ApplicationCommands.Save)
            {
                this.DialogResult = true;
                this.PromptResult = PromptValue.Text;
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.PromptValue.Focus();
        }
    }
}
