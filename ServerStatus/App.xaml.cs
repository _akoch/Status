﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Reflection;

namespace ServerStatus
{
    public partial class App : Application
    {
        public static Settings Settings = Settings.Load();
        public static Database Database = new Database();
        public static Daemon Daemon = new Daemon();
        public static Holder Holder = new Holder();

        public App()
        {
            Attribute displayNameAttr = null;

            foreach (Type t in Assembly.GetExecutingAssembly().GetTypes()) {
                if (typeof(Pinger).IsAssignableFrom(t) && (displayNameAttr = t.GetCustomAttribute(typeof(DisplayNameAttribute), false)) != null)
                {
                    Holder.PingerTypeLabels.Add(t, (displayNameAttr as DisplayNameAttribute).DisplayName);
                }
            }
        }
    }
}
