﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace ServerStatus
{
    [Serializable]
    [DisplayName("TCP")]
    public class TCPPinger : PortPinger
    {
        protected PingerResult _connectionResult = null;

        protected void AttemptTCPConnection(object threadStartParam)
        {
            if (!(threadStartParam is Tuple<string, int>))
            {
                _connectionResult = new PingerResult(false, "Daemon error - TCPPinger.AttemptTCPConnection threadStartParam is not Tuple<string, int>");
                return;
            }

            Tuple<string, int> threadStartParams = threadStartParam as Tuple<string, int>;
            Stopwatch stopwatch = Stopwatch.StartNew();

            try
            {
                using (TcpClient client = new TcpClient())
                {
                    client.Connect(threadStartParams.Item1, threadStartParams.Item2);
                    stopwatch.Stop();
                }

                _connectionResult = new PingerResult(true, (int)stopwatch.ElapsedMilliseconds);
            }
            catch (SocketException se)
            {
                _connectionResult = new PingerResult(false, "Connection attempt was halted by a SocketException: " + se.Message);
            }
            catch (ThreadAbortException) {

            }
            catch (Exception ex)
            {
                _connectionResult = new PingerResult(false, "Daemon error - unhandled " + ex.GetType().FullName + ": " + ex.Message);
            }
        }

        protected override PingerResult AttemptContact()
        {
            ParameterizedThreadStart tcpAttemptThreadStart = new ParameterizedThreadStart(AttemptTCPConnection);
            Thread tcpAttemptThread = new Thread(tcpAttemptThreadStart);
            
            _connectionResult = null;
            tcpAttemptThread.Start(new Tuple<string, int>(Hostname, (int)Port));
            tcpAttemptThread.Join(Timeout * 1000);

            PingerResult attemptResult = _connectionResult;
            if (attemptResult == null) {
                tcpAttemptThread.Abort();
                return new PingerResult(false, "Software timeout (thread timeout)");
            }
            else {
                return attemptResult;
            }
        }
    }
}
