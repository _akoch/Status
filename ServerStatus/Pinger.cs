﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Reflection;

namespace ServerStatus
{
    public abstract class Pinger : INotifyPropertyChanged
    {
        #region Boilerplate

        private int _id = 0;
        private string _hostname = String.Empty;
        private ushort _timeout = 30;
        private string _label = String.Empty;
        private string _name = String.Empty;

        private PingerResult _lastResult = null;
        public Server Parent = null;

        public Pinger()
        {
            this.Label = (this.GetType().GetCustomAttribute(typeof(DisplayNameAttribute), false) as DisplayNameAttribute).DisplayName;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
                App.Database.UpdatePinger(this);
            }
        }
        #endregion

        #region Properties
        public string Hostname
        {
            get
            {
                return _hostname;
            }

            set
            {
                _hostname = value;
                OnPropertyChanged("hostname");
            }
        }

        public ushort Timeout
        {
            get
            {
                return _timeout;
            }

            set
            {
                _timeout = value;
                OnPropertyChanged("Timeout");
            }
        }

        public PingerResult LastResult
        {
            get
            {
                return _lastResult;
            }

            protected set
            {
                _lastResult = value;
                OnPropertyChanged("LastResult");
            }
        }

        public string Label
        {
            get
            {
                return _label;
            }

            protected set
            {
                _label = value;
                OnPropertyChanged("Label");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public int ID
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged("ID");
            }
        }
        #endregion

        protected abstract PingerResult AttemptContact();

        public void Process()
        {
            this.LastResult = this.AttemptContact();
        }
    }
}
