﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace ServerStatus
{
    public class Holder : INotifyPropertyChanged
    {
        private ObservableCollection<Server> _servers = null;
        private Dictionary<Type, string> _pingerTypes = new Dictionary<Type, string>();
        private string _log = String.Empty;

        public Holder()
        {
            this.Servers = new ObservableCollection<Server>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ObservableCollection<Server> Servers
        {
            get
            {
                return _servers;
            }

            set
            {
                _servers = value;
                OnPropertyChanged("Servers");

                if (value != null)
                {
                    _servers.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs args) =>
                    {
                        OnPropertyChanged("Servers");
                    };
                }
            }
        }

        public List<String> PingerLabels
        {
            get
            {
                return _pingerTypes.Values.ToList<String>();
            }
        }

        public List<String> PingerTypes
        {
            get
            {
                return _pingerTypes.Keys.Select<Type, string>(t => t.FullName).ToList<string>();
            }
        }

        public Dictionary<Type, string> PingerTypeLabels
        {
            get
            {
                return _pingerTypes;
            }
        }

        public string Log
        {
            get
            {
                return _log;
            }

            set
            {
                _log = value;
                OnPropertyChanged("Log");
            }
        }

    }
}
