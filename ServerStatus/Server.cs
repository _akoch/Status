﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Data;

namespace ServerStatus
{
    public enum Status : int {
        FullOutage = 0,
        PartialOutage = 1,
        Online = 2,
        NotAvailable = 3,
        QueryInProgress = 4
    }

    public class Server : INotifyPropertyChanged
    {
        private Status _status = Status.NotAvailable;
        private string _name = "Unnamed Server";
        private uint _id = 0;
        private ObservableCollection<Pinger> _pingers = new ObservableCollection<Pinger>();
        private bool _beingSaved = false;
        private bool _beingLoaded = true;
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void Autosave()
        {
            if (!_beingLoaded)
            {
                IsBeingSaved = true;
                App.Database.UpdateServer(this);
            }
        }

        public void EnableSaving()
        {
            _beingLoaded = false;
        }

        public string Name
        {
            get {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
                Autosave();
            }
        }
        
        public uint ID
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged("ID");
            }
        }

        public Status Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public ObservableCollection<Pinger> Pingers
        {
            get
            {
                return _pingers;
            }

            protected set
            {
                _pingers = value;
                OnPropertyChanged("Pingers");
            }
        }

        public bool IsBeingSaved
        {
            get
            {
                return _beingSaved;
            }

            set
            {
                _beingSaved = value;
                OnPropertyChanged("IsBeingSaved");
            }
        }
    }

    public class StatusToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() == typeof(Status)) {
                switch ((Status)value)
                {
                    case Status.Online:
                        return "Online";
                    
                    case Status.PartialOutage:
                        return "Partial Outage";
                    
                    case Status.FullOutage:
                        return "Full Outage";

                    case Status.QueryInProgress:
                        return "Querying..";

                    case Status.NotAvailable:
                    default:
                        return "Status Not Available";
                }
            }
            else
            {
                return "Status Not Available";
            }
        }

        public object ConvertBack(object value, Type sourceType, object parameter, CultureInfo culture)
        {
            switch (value.ToString())
            {
                case "Online":
                    return Status.Online;

                case "Partial Outage":
                    return Status.PartialOutage;

                case "Full Outage":
                    return Status.FullOutage;

                case "Querying":
                    return Status.QueryInProgress;

                case "Status Not Available":
                default:
                    return Status.NotAvailable;
            }
        }
    }
}
