﻿using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.IO;

namespace ServerStatus
{
    [Serializable]
    public class Settings : INotifyPropertyChanged
    {
        [XmlIgnore]
        private static string filepath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +  "/serverstatus.xml";

        [XmlIgnore]
        private static XmlSerializer serializer;

        public event PropertyChangedEventHandler PropertyChanged;

        [XmlIgnore]
        private bool hasFinishedLoading = false;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

            this.Save();
        }

        private static XmlSerializer Serializer
        {
            get
            {
                if (serializer == null)
                {
                    serializer = new XmlSerializer(typeof(Settings));
                }

                return serializer;
            }
        }

        public static Settings Load()
        {
            if (!File.Exists(filepath))
            {
                return new Settings()
                {
                    hasFinishedLoading = true
                };
            }

            FileStream f = null;
            Settings ret = null;

            try
            {
                f = File.Open(filepath, FileMode.Open);
                ret = (Settings)Serializer.Deserialize(f);
            }
            catch
            {
                ret = new Settings();
            }

            if (f != null)
            {
                f.Close();
            }

            ret.hasFinishedLoading = true;
            return ret;
        }

        public void Save()
        {
            if (!this.hasFinishedLoading)
            {
                return;
            }

            FileStream f = null;

            try
            {
                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }

                f = File.Open(filepath, FileMode.CreateNew);
                Serializer.Serialize(f, this);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.GetType().FullName + "\n" + e.Message, "An error has occurred while saving ServerStatus settings.", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            if (f != null)
            {
                f.Close();
            }
        }

        private string _host = "localhost";
        private string _user = "root";
        private string _pass = "secure";
        private string _name = "serverstatus";
        private uint _interval = 30;
        private bool _autostart = true;

        public string MysqlHost
        {
            get
            {
                return _host;
            }

            set
            {
                _host = value;
                OnPropertyChanged("MysqlHost");
            }
        }

        public string MysqlUser
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
                OnPropertyChanged("MysqlUser");
            }
        }

        public string MysqlPass
        {
            get
            {
                return _pass;
            }

            set
            {
                _pass = value;
                OnPropertyChanged("MysqlPass");
            }
        }

        public string MysqlName
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("MysqlName");
            }
        }

        public uint Interval
        {
            get
            {
                return _interval;
            }

            set
            {
                _interval = value;
                OnPropertyChanged("Interval");
            }
        }

        public bool Autostart
        {
            get
            {
                return _autostart;
            }

            set
            {
                _autostart = value;
                OnPropertyChanged("Autostart");
            }
        }
    }
}
