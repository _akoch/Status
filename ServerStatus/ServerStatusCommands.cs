﻿using System;
using System.Windows;
using System.Windows.Input;

namespace ServerStatus
{
    public static class ServerStatusCommands
    {
        public static readonly RoutedUICommand StartDaemon = new RoutedUICommand(
            "Start Daemon",
            "StartDaemon",
            typeof(ServerStatusCommands),
            new InputGestureCollection() {
                new KeyGesture(Key.F5, ModifierKeys.Control)
            }
        );

        public static readonly RoutedUICommand StopDaemon = new RoutedUICommand(
            "Stop Daemon",
            "StopDaemon",
            typeof(ServerStatusCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F5, ModifierKeys.Control | ModifierKeys.Alt)
            }
        );
    }
}
